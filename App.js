import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import TrackPlayer, {
  useTrackPlayerEvents,
  Event,
} from 'react-native-track-player';
import {useProgress} from 'react-native-track-player';
import Scrubber from 'react-native-scrubber';

const timeBefore = 20;

const durations = [
  1177.3648979591837, // 19:36 - Works
  2420.1404081632654, // 40:19 - Works
  3549.126530612245, // 59:08 - Works
  3934.1975510204084, // 01:05:40 - Doesn't work
  4977.502040816326, // 1:24:56 - Doesn't work
  5415.758367346939, // 1:30:14 - Doesn't work
  8872.724, // 2:27:55 - Doesn't work
];

const songs = [
  'https://www.edgargrs.com/audios/song_0.mp3', // Works
  'https://www.edgargrs.com/audios/song_1.mp3', // Works
  'https://www.edgargrs.com/audios/song_2.mp3', // Works
  'https://www.edgargrs.com/audios/song_3.mp3', // Doesn't work
  'https://www.edgargrs.com/audios/song_4.mp3', // Doesn't work
  'https://www.edgargrs.com/audios/song_5.mp3', // Doesn't work
  'https://www.edgargrs.com/audios/song_6.mp3',
];

// const songs = [
//   require('./audios/song_0.mp3'),
//   require('./audios/song_1.mp3'),
//   require('./audios/song_2.mp3'),
//   require('./audios/song_3.mp3'),
//   require('./audios/song_4.mp3'),
//   require('./audios/song_5.mp3'),
// ];

const playSong = async (n = 0) => {
  let song = `song_${n}`;
  await TrackPlayer.reset();
  const track = {
    id: song,
    url: songs[n],
    title: song,
    artist: song,
    album: song,
    duration: durations[n],
  };
  await TrackPlayer.add([track]);
  await TrackPlayer.play();
};

const renderAudioButtons = () => {
  return [...Array(7).keys()].map(i => {
    return (
      <Button key={i} title={`Play Song ${i}`} onPress={() => playSong(i)} />
    );
  });
};

const ScrubberComponent = () => {
  const [track, setTrack] = useState();
  const {position, buffered, duration} = useProgress();

  const play = async () => await TrackPlayer.play();

  const pause = async () => await TrackPlayer.pause();

  const fastForward = async () => {
    await TrackPlayer.pause();
    let id = track.id.split('_')[1];
    let seek = durations[id] - timeBefore;
    console.log('fastForward', seek);
    await TrackPlayer.seekTo(seek);
    await TrackPlayer.play();
  };

  const onSlidingComplete = async seconds => {
    console.log('onSlidingComplete', seconds);
    await TrackPlayer.pause();
    await TrackPlayer.seekTo(seconds);
    await TrackPlayer.play();
  };

  useTrackPlayerEvents([Event.PlaybackTrackChanged], async event => {
    if (event.type === Event.PlaybackTrackChanged && event.nextTrack != null) {
      const t = await TrackPlayer.getTrack(event.nextTrack);
      setTrack(t);
    }
  });

  console.log('POSITION: ' + position, 'DURATION: ' + duration);

  return (
    <View style={styles.sectionContainer}>
      <View style={styles.valuesContainer}>
        <Text style={styles.valueStyle}>
          Position{'\r\n'}
          {position}
        </Text>
        <Text style={styles.valueStyle}>
          Current Sound{'\r\n'}
          {track ? track.title : ''}
        </Text>
        <Text style={styles.valueStyle}>
          Duration{'\r\n'}
          {duration}
        </Text>
      </View>
      <Scrubber
        value={position}
        onSlidingComplete={onSlidingComplete}
        onSlidingStart={console.log}
        totalDuration={duration}
        bufferedValue={buffered}
      />
      <View style={styles.controlsContainer}>
        <Button title={'Play'} onPress={play} />
        <Button title={'Pause'} onPress={pause} />
        <Button title={timeBefore + ' to finish'} onPress={fastForward} />
      </View>
      <View style={styles.buttonsContainer}>{renderAudioButtons()}</View>
    </View>
  );
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const setUpTrackPlayer = async () => {
    try {
      await TrackPlayer.setupPlayer();
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    setUpTrackPlayer();
    playSong();
    return async () => await TrackPlayer.destroy();
  }, []);

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        style={backgroundStyle}
        contentContainerStyle={styles.container}>
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <ScrubberComponent />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
    height: '100%',
  },
  controlsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 10,
  },
  buttonsContainer: {
    height: '50%',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  buttonStyle: {
    marginVertical: 5,
  },
  valuesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  valueStyle: {
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default App;
