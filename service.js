import TrackPlayer from 'react-native-track-player';

module.exports = async function () {
  TrackPlayer.addEventListener('remote-play', async () =>
    console.log('remote-play'),
  );

  TrackPlayer.addEventListener('remote-pause', async () =>
    console.log('remote-pause'),
  );
};
